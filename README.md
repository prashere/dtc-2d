# Dodge The Creeps 2D (DTC-2D)

Dodge The Creeps 2D is a 2D game where the player has to dodge / escape the **Creeps**.

This game is developed using [Godot](https://godotengine.org), a **F**ree and **O**pen **S**ource game engine. In fact DTC-2D is one of the game in the Godot's official tutorial.

This is one of the adaption from the official tutorial built from scratch and has it's own deviations and roadmap.

### Features
 - [x] Basic Game Play
 - [x] Virtual Joystick for Smart Phones
 - [x] Vibrate on Game Over (compatible devices)
 - [ ] Game Pause
 - [ ] Game Levels
 - [ ] High Scores
 - [ ] Special Powers
