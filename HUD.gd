extends CanvasLayer

signal start_game
signal quit_game


func update_score(score):
	$ScoreLabel.text = str(score)


func show_message(text):
	$MessageLabel.text = text
	$MessageLabel.show()
	$MessageTimer.start()


func show_gameover():
	show_message("Game Over")
	yield($MessageTimer, "timeout")
	$MessageLabel.text = "Dodge the Creeps"
	$MessageLabel.show()
	$Button.show()
	$QuitButton.show()


func _on_Button_pressed() -> void:
	$Button.hide()
	$QuitButton.hide()
	emit_signal("start_game")


func _on_MessageTimer_timeout() -> void:
	$MessageLabel.hide()


func _on_QuitButton_pressed() -> void:
	emit_signal("quit_game")
