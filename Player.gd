extends Area2D

signal hit

export var speed = 400.0  # pixels/second
var screen_size = Vector2.ZERO


func _ready():
	screen_size = get_viewport_rect().size
	hide()


func _process(delta: float) -> void:
	# Virtual Joystick based movement
	var move_vector = Vector2.ZERO
	move_vector.x = Input.get_axis("ui_left", "ui_right")
	move_vector.y = Input.get_axis("ui_up", "ui_down")

	# Play Animation only when position is changing
	$AnimatedSprite.animation = "right"
	if move_vector != Vector2.ZERO:
		$AnimatedSprite.play()
	else:
		$AnimatedSprite.stop()

	# flip vertically when moving down
	$AnimatedSprite.flip_v = move_vector.y > 0
	# flip horizontally when moving left
	$AnimatedSprite.flip_h = move_vector.x < 0

	position += move_vector * speed * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)


func start(new_position):
	position = new_position
	show()
	$CollisionShape2D.disabled = false


func _on_Player_body_entered(body: Node) -> void:
	hide()
	$CollisionShape2D.set_deferred("disabled", true)
	emit_signal("hit")
